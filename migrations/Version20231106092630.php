<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231106092630 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE hall DROP FOREIGN KEY FK_1B8FA83FB4CB84B6');
        $this->addSql('ALTER TABLE screening DROP FOREIGN KEY FK_B708297DB4CB84B6');
        $this->addSql('DROP TABLE cinema');
        $this->addSql('DROP INDEX IDX_1B8FA83FB4CB84B6 ON hall');
        $this->addSql('ALTER TABLE hall DROP cinema_id');
        $this->addSql('DROP INDEX IDX_B708297DB4CB84B6 ON screening');
        $this->addSql('ALTER TABLE screening DROP cinema_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cinema (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, address VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE hall ADD cinema_id INT NOT NULL');
        $this->addSql('ALTER TABLE hall ADD CONSTRAINT FK_1B8FA83FB4CB84B6 FOREIGN KEY (cinema_id) REFERENCES cinema (id)');
        $this->addSql('CREATE INDEX IDX_1B8FA83FB4CB84B6 ON hall (cinema_id)');
        $this->addSql('ALTER TABLE screening ADD cinema_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE screening ADD CONSTRAINT FK_B708297DB4CB84B6 FOREIGN KEY (cinema_id) REFERENCES cinema (id)');
        $this->addSql('CREATE INDEX IDX_B708297DB4CB84B6 ON screening (cinema_id)');
    }
}
