<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231106100821 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE screening DROP FOREIGN KEY FK_B708297D52AFCFD6');
        $this->addSql('ALTER TABLE screening DROP FOREIGN KEY FK_B708297D8F93B6FC');
        $this->addSql('DROP INDEX UNIQ_B708297D52AFCFD6 ON screening');
        $this->addSql('DROP INDEX UNIQ_B708297D8F93B6FC ON screening');
        $this->addSql('ALTER TABLE screening DROP movie_id, DROP hall_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE screening ADD movie_id INT NOT NULL, ADD hall_id INT NOT NULL');
        $this->addSql('ALTER TABLE screening ADD CONSTRAINT FK_B708297D52AFCFD6 FOREIGN KEY (hall_id) REFERENCES hall (id)');
        $this->addSql('ALTER TABLE screening ADD CONSTRAINT FK_B708297D8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B708297D52AFCFD6 ON screening (hall_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B708297D8F93B6FC ON screening (movie_id)');
    }
}
