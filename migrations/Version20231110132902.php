<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231110132902 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE payments ADD screening_id INT NOT NULL');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B3270F5295D FOREIGN KEY (screening_id) REFERENCES screening (id)');
        $this->addSql('CREATE INDEX IDX_65D29B3270F5295D ON payments (screening_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B3270F5295D');
        $this->addSql('DROP INDEX IDX_65D29B3270F5295D ON payments');
        $this->addSql('ALTER TABLE payments DROP screening_id');
    }
}
