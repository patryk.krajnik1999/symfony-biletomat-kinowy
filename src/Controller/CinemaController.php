<?php

namespace App\Controller;

use App\Entity\Payments;
use App\Repository\PaymentsRepository;
use App\Repository\ScreeningRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;

class CinemaController extends AbstractController
{
    private ScreeningRepository $screeningRepository;
    private PaymentsRepository $paymentsRepository;

    public function __construct(
        ScreeningRepository $screeningRepository, PaymentsRepository $paymentsRepository
    ) {
        $this->screeningRepository = $screeningRepository;
        $this->paymentsRepository = $paymentsRepository;
    }

    #[Route('/screenings', name: 'app_screenings', methods: ['GET'])]
    public function works(): JsonResponse
    {
        $allScreenings = $this->screeningRepository->findAll();
        $screenings    = [];
        for ($i = 0; $i < count($allScreenings); $i++) {
            $screening    = $allScreenings[$i];
            $data    = [
                'id'          => $screening->getId(),
                'dateTime'       => $screening->getDateTime(),
                'movieTitle' => $screening->getMovie()->getTitle(),
                'movieDescription'    => $screening->getMovie()->getDescription(),
                'movieCategories'   => $screening->getMovie()->getCategories(),
                'movieDuration'      => $screening->getMovie()->getDuration(),
                'movieAccessAge'     => $screening->getMovie()->getAccessAge(),
                'hallNumber'    => $screening->getHall()->getNumber(),
                'hallColumns'  => $screening->getHall()->getColumns(),
                'hallRows'  => $screening->getHall()->getRows(),
                'seats' => $screening->getSeats(),
            ];
            $screenings[] = $data;
        }

        return $this->json($screenings);
    }

    #[Route('/takeSeats', name: 'app_setStatus', methods: ['POST'])]
    public function takeSeats(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();
            $content = json_decode($request->getContent(), true);
            $screening = $this->screeningRepository->findOneBy(['id' => $content['screeningId']]);

            $seats = $screening->getSeats();
            for($i = 0; $i < $content['seatsNumber']; $i++) {
                $seats[$content['seats'][$i]] = 1;
            }
            $screening->setSeats($seats);
            $entityManager->persist($screening);

            $entityManager->flush();

//            sleep(60);
//
//            $payed = $this->paymentsRepository->findOneBy(['seats' => $content['seats']]);
//            if ($payed == null) {
//                $seats = $screening->getSeats();
//                for($i = 0; $i < $content['seatsNumber']; $i++) {
//                    $seats[$content['seats'][$i]] = 0;
//                }
//                $screening->setSeats($seats);
//            }

        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }

    #[Route('/pay', name: 'app_pay', methods: ['POST'])]
    public function pay(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();
            $content = json_decode($request->getContent(), true);

            $screening = $this->screeningRepository->findOneBy(['id' => $content['screeningId']]);

            $payment = new Payments();
            $date = new \DateTime();
            $date->format('d-m-Y');
            $payment->setDateTime($date);
            $payment->setCost($content['cost']);
            $payment->setSeats($content['seats']);
            $payment->setScreening($screening);
            $entityManager->persist($payment);

            $entityManager->flush();
        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($content);
    }

    #[Route('/checkSeats', name: 'app_checkSeats', methods: ['POST'])]
    public function checkSeats(
        ManagerRegistry $doctrine,
        Request $request
    ): JsonResponse {
        try {
            $entityManager = $doctrine->getManager();
            $content = json_decode($request->getContent(), true);
            $screening = $this->screeningRepository->findOneBy(['id' => $content['screeningId']]);

            $payed = false;
            $payments = $screening->getPayments();

            $j = 0;
            foreach($payments as $pay) {
                $seats = $pay->getSeats();
                for($i = 0; $i < $content['seatsNumber']; $i++) {
                    $seat = $content['seats'][$i];
                    if (in_array($seat, $seats)) {
                        $j += 1;
                    };
                }
                if ($j == $content['seatsNumber']) {
                    $payed = true;
                    break;
                } else {
                    $j = 0;
                }
            }

            if (!$payed) {
                $seats = $screening->getSeats();
                for($i = 0; $i < $content['seatsNumber']; $i++) {
                    $seats[$content['seats'][$i]] = 0;
                }
                $screening->setSeats($seats);
                $entityManager->persist($screening);

                $entityManager->flush();
            }

        } catch (Exception $ex) {
            return $this->json($ex);
        }

        return $this->json($payed);
    }

}
