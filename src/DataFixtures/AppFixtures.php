<?php

namespace App\DataFixtures;

use App\Entity\Hall;
use App\Entity\Movie;
use App\Entity\Screening;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function getSeatsArray(int $rows, int $columns): array
    {
        $seats = [];
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $columns; $j++) {
                $seats[] = 0;
            }
        }
        return $seats;
    }


    public function load(ObjectManager $manager): void
    {
        $movie = new Movie();
        $movie->setTitle('CHŁOPI');
        $movie->setDescription('„CHŁOPI” zostaną zrealizowani w technice animacji malarskiej, która podbiła serca fanów na całym świecie przy poprzedniej produkcji studia – filmie „Twój Vincent”. Historia filmu oparta jest na powieści Władysława Reymonta, który w 1924 roku otrzymał Literacką Nagrodę Nobla wyprzedzając Thomasa Manna, George\'a Bernarda Shawa i Thomasa Hardy\'ego.');
        $movie->setDuration(116);
        $movie->setAccessAge(13);
        $movie->setCategories(['Dramat', 'Animowany']);
        $manager->persist($movie);

        $movie2 = new Movie();
        $movie2->setTitle('CZAS KRWAWEGO KSIĘŻYCA');
        $movie2->setDescription('Produkcja opowiada o serii tajemniczych zaginięć członków bogatego plemienia Osagów mieszkających na obszarach złóż ropy naftowej w Oklahomie. Sprawą zaczyna się interesować nowo powstała agencja federalna FBI w tym agent J. Edgar Hoover.');
        $movie2->setDuration(206);
        $movie2->setAccessAge(15);
        $movie2->setCategories(['Dramat', 'Kryminał']);
        $manager->persist($movie2);

        $movie3 = new Movie();
        $movie3->setTitle('PATI I KLĄTWA POSEJDONA');
        $movie3->setDescription('Wielka przygoda małych bohaterów! Nieustraszona mysz Pati i jej koci przyjaciel Sam wyruszają w pełną przygód podróż po starożytnej Grecji. Wraz ze swoimi przyjaciółmi przemierzą burzliwe morza i zmierzą się z mitologicznymi stworami, podejmując wyzwanie samego Posejdona.');
        $movie3->setDuration(95);
        $movie3->setAccessAge(6);
        $movie3->setCategories(['Animowany', 'Familijny', 'Przygodowy']);
        $manager->persist($movie3);


        $hall = new Hall();
        $hall->setNumber(1);
        $hall->setRows(5);
        $hall->setColumns(10);
        $manager->persist($hall);

        $hall2 = new Hall();
        $hall2->setNumber(2);
        $hall2->setRows(7);
        $hall2->setColumns(12);
        $manager->persist($hall2);

        $hall3 = new Hall();
        $hall3->setNumber(3);
        $hall3->setRows(10);
        $hall3->setColumns(15);
        $manager->persist($hall3);


        $screening = new Screening();
        $date = new DateTime('2023-11-12 12:00:00');
        $date->format('d-m-Y');
        $screening->setDateTime($date);
        $screening->setHall($hall);
        $screening->setMovie($movie3);
        $screening->setSeats($this->getSeatsArray($hall->getRows(), $hall->getColumns()));
        $manager->persist($screening);

        $screening2 = new Screening();
        $date = new DateTime('2023-11-12 17:00:00');
        $date->format('d-m-Y');
        $screening2->setDateTime($date);
        $screening2->setHall($hall3);
        $screening2->setMovie($movie2);
        $screening2->setSeats($this->getSeatsArray($hall3->getRows(), $hall3->getColumns()));
        $manager->persist($screening2);

        $screening3 = new Screening();
        $date = new DateTime('2023-11-13 11:00:00');
        $date->format('d-m-Y');
        $screening3->setDateTime($date);
        $screening3->setHall($hall);
        $screening3->setMovie($movie3);
        $screening3->setSeats($this->getSeatsArray($hall->getRows(), $hall->getColumns()));
        $manager->persist($screening3);

        $screening4 = new Screening();
        $date = new DateTime('2023-11-13 13:00:00');
        $date->format('d-m-Y');
        $screening4->setDateTime($date);
        $screening4->setHall($hall2);
        $screening4->setMovie($movie);
        $screening4->setSeats($this->getSeatsArray($hall2->getRows(), $hall2->getColumns()));
        $manager->persist($screening4);

        $screening5 = new Screening();
        $date = new DateTime('2023-11-13 16:00:00');
        $date->format('d-m-Y');
        $screening5->setDateTime($date);
        $screening5->setHall($hall3);
        $screening5->setMovie($movie2);
        $screening5->setSeats($this->getSeatsArray($hall3->getRows(), $hall3->getColumns()));
        $manager->persist($screening5);

        $screening6 = new Screening();
        $date = new DateTime('2023-11-14 12:00:00');
        $date->format('d-m-Y');
        $screening6->setDateTime($date);
        $screening6->setHall($hall2);
        $screening6->setMovie($movie);
        $screening6->setSeats($this->getSeatsArray($hall2->getRows(), $hall2->getColumns()));
        $manager->persist($screening6);

        $screening7 = new Screening();
        $date = new DateTime('2023-11-12 15:00:00');
        $date->format('d-m-Y');
        $screening7->setDateTime($date);
        $screening7->setHall($hall2);
        $screening7->setMovie($movie2);
        $screening7->setSeats($this->getSeatsArray($hall2->getRows(), $hall2->getColumns()));
        $manager->persist($screening7);

        $screening8 = new Screening();
        $date = new DateTime('2023-11-13 12:00:00');
        $date->format('d-m-Y');
        $screening8->setDateTime($date);
        $screening8->setHall($hall3);
        $screening8->setMovie($movie);
        $screening8->setSeats($this->getSeatsArray($hall3->getRows(), $hall3->getColumns()));
        $manager->persist($screening8);

        $screening8 = new Screening();
        $date = new DateTime('2023-11-13 14:00:00');
        $date->format('d-m-Y');
        $screening8->setDateTime($date);
        $screening8->setHall($hall);
        $screening8->setMovie($movie);
        $screening8->setSeats($this->getSeatsArray($hall->getRows(), $hall->getColumns()));
        $manager->persist($screening8);

        $manager->flush();
    }
}
